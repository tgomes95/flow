# Flow

Arduino code and Python scripts to automatically refill a bowl of water and send the data to SAP Cloud Platform

## Instructions

### Install Arduino IDE


```
#!bash

# Install Arduino IDE
sudo apt-get install arduino arduino-core arduino-mk

# Add user to dialout
# Ex.: sudo gpasswd -a sap dialout
sudo gpasswd -a $USER dialout
```

### Setup Arduino IDE

+ Open Arduino IDE
+ Select the Arduino board in _Tools > Board > Arduino Uno_
+ Select the port in _Tools > Serial Port > Port_
+ For each folder inside the _libraries/_ folder, add it in _Sketch > Import library... > Add library..._
+ Open the _code/code.ino_ file in _Sketch > Add file > smartplant.ino_
+ Upload the code to the Arduino board

### Run the following commands


```
#!bash

cd code/
python send_data.sh
```
