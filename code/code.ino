#include <SR04.h>

#define ECHO_PIN 11
#define TRIG_PIN 12

#define RELAY_PIN 6
#define RELAY_ON  HIGH
#define RELAY_OFF LOW

long distance;
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);

void setup() {
   Serial.begin(9600);
   delay(1000);
}

void loop() {
   distance = sr04.Distance();

   Serial.println(distance);

   if (distance >= 7) {
       WaterPumpOn();
   }
   else {
       WaterPumpOff();
   }

   delay(1000);
}

void WaterPumpOn()
{
    digitalWrite(RELAY_PIN,RELAY_ON);
}

void WaterPumpOff()
{
    digitalWrite(RELAY_PIN,RELAY_OFF);
}
