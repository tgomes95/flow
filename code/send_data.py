import serial
import datetime

import requests

def send_data(timestamp, distance, volume):
    API = 'https://presalesbrbc871ee72.us1.hana.ondemand.com/201710_FLOW/table_insert.xsjs?'

    print '[%s] Sending message...' % (timestamp)
    try:
        r = requests.get(API + 'timestamp=%s&distance=%d&volume=%d' % (timestamp.replace(' ', '%20'), distance, volume))
    except requests.ConnectionError:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
    else:
        print '[%s] Message sent\n' % (timestamp)
        print '%d' % (r.status_code)

def main():
    ser = serial.Serial('/dev/ttyS0')

    ser.readline()

    while True:
        timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S:%f')
        distance = int(ser.readline())
        volume = (12 - distance) * 144
        send_data(timestamp, distance, volume)

if __name__ == '__main__':
	main()
